## Arbitrary notes
- download `arkcase-ce` vagrant box, around 6.4GB (https://app.vagrantup.com/arkcase/boxes/arkcase-ce/versions/3.3.1-r1-a)
  ```
  vagrant box add arkcase/arkcase-ce --box-version 3.3.1-r1-a
  ```

- start it up :cactus:
  ```
  vagrant up
  ```

- download and manually add a box, e.g. we have a box file named `d4fbb83b-e127-4611-9029-e40d0129aa9a` downloaded from https://app.vagrantup.com/arkcase/boxes/arkcase-ce/versions/3.3.1/providers/virtualbox.box
  ```
  vagrant box add d4fbb83b-e127-4611-9029-e40d0129aa9a --name arkcase/arkcase-ce
  ```
- the caveat is we can't specify the box version, thus it would be version `0`! :panda_face:

  Let's do some magic trick, update it to the correct version (but I'm not sure if there's any further issue :crocodile:)
  ```
  mv ~/.vagrant.d/boxes/arkcase-VAGRANTSLASH-arkcase-ce/0 ~/.vagrant.d/boxes/arkcase-VAGRANTSLASH-arkcase-ce/3.3.1
  ```
